import Vue from 'vue'
import App from './App.vue'
import * as firebase from 'firebase/app'
import "firebase/auth"
import 'firebase/database'

const firebaseConfig = {
  //firebase APIkey
}; 

Vue.config.productionTip = false
firebase.initializeApp(firebaseConfig);

new Vue({
  render: h => h(App),
}).$mount('#app')
